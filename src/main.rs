/**
Copyright (C) 2021 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use anyhow::Result;
use matrix_sdk::{
    room::{Common, Room},
    ruma::{
        events::{
            room::{
                member::{MemberEventContent, MembershipChange},
                message::MessageEventContent,
            },
            AnyMessageEventContent, StateEvent, SyncStateEvent,
        },
        user_id, RoomId, UserId,
    },
    Client, ClientConfig, SyncSettings,
};
use serde::Deserialize;
use std::convert::TryFrom;
use std::sync::Arc;
use tokio::fs;
use tokio::sync::RwLock;
use tracing::{debug, error, info};
use tracing_subscriber::EnvFilter;

mod collections;

/// Reasons from an IRC appservice to ignore
const IRC_IGNORE_REASONS: [&str; 5] = [
    // When a user uses /part on the IRC side
    "user left",
    // Leaving the IRC network via !quit
    "issued !quit command",
    // 30 day bridge inactivity kick
    "You have been kicked for being idle",
    // Issues when joining
    "Couldn't connect you to this channel. Please try again later.",
    "IRC connection failure.",
];

#[derive(Clone)]
struct State {
    config: Config,
    recent_kicks: Arc<RwLock<collections::ExpiringSet>>,
}

#[derive(Clone, Deserialize)]
struct Config {
    username: String,
    password: String,
    report_room: String,
}

/// Get a room's canonical alias or fallback to
/// the internal ID if it doesn't have one
fn room_alias(room: &Common) -> String {
    match room.canonical_alias() {
        Some(alias) => alias.to_string(),
        None => room.room_id().to_string(),
    }
}

/// Get the rooms that we're in that another user is in
/// TODO: if rooms haven't been loaded yet, this can be
/// super slow
async fn get_rooms_user_is_in(client: &Client, user: &str) -> Vec<String> {
    let mut handles = vec![];
    for room in client.joined_rooms() {
        handles.push(tokio::spawn(async move {
            let members = match room.joined_members().await {
                Ok(members) => members,
                Err(err) => {
                    error!(error = ?err, room = %room.room_id(), "Unable to fetch room members");
                    vec![]
                }
            };
            (room, members)
        }));
    }
    let mut rooms = vec![];
    let user = user.to_string();
    for handle in handles {
        let (room, members) = handle.await.unwrap();
        for member in members {
            if *member.user_id().to_string() == user {
                rooms.push(room_alias(&room));
            }
        }
    }
    rooms
}

async fn handle_event(
    ev: SyncStateEvent<MemberEventContent>,
    client: Client,
    room: Room,
    state: State,
) {
    let event: StateEvent<_> = ev.into_full_event(room.room_id().clone());
    debug!(event = ?event);
    let reason = event
        .content
        .reason
        .clone()
        .unwrap_or_else(|| "no reason provided".to_string());
    match event.membership_change() {
        MembershipChange::Joined => {
            let joined = event.sender.to_string();
            if state.recent_kicks.read().await.contains(&joined) {
                let content = AnyMessageEventContent::RoomMessage(
                    MessageEventContent::text_plain(format!(
                        "After being recently kicked, {} joined {}",
                        &joined,
                        room_alias(&room),
                    )),
                );
                let send_room = client.get_joined_room(
                    &RoomId::try_from(state.config.report_room.as_str())
                        .unwrap(),
                );
                if let Some(room) = send_room {
                    info!(
                        room = %room.room_id(),
                        content = ?content,
                        "Sending notification"
                    );
                    room.send(content, None).await.unwrap();
                } else {
                    error!(
                        report_room = %state.config.report_room,
                        "Not joined to report_room"
                    );
                }
            }
        }
        MembershipChange::Kicked => {
            // TODO: This should be configurable for other bridges, e.g. OFTC
            if event.sender == user_id!("@appservice:libera.chat")
                && IRC_IGNORE_REASONS.contains(&reason.as_str())
            {
                debug!("Ignoring kick by IRC appservice");
                return;
            }
            // The state_key is the full user id of the kicked user
            let kicked = event.state_key;
            state.recent_kicks.write().await.add(&kicked);
            // Get a list of what other rooms they're in
            let other_rooms = get_rooms_user_is_in(&client, &kicked).await;
            let other_rooms_str = if other_rooms.is_empty() {
                "".to_string()
            } else {
                format!(", they are still in {}", other_rooms.join(", "))
            };
            let content = AnyMessageEventContent::RoomMessage(
                MessageEventContent::text_plain(format!(
                    "{} was kicked by {} from {} ({}){}",
                    &kicked,
                    event.sender,
                    room_alias(&room),
                    reason,
                    other_rooms_str
                )),
            );
            let send_room = client.get_joined_room(
                &RoomId::try_from(state.config.report_room.as_str()).unwrap(),
            );
            if let Some(room) = send_room {
                info!(
                    room = %room.room_id(),
                    content = ?content,
                    "Sending notification"
                );
                room.send(content, None).await.unwrap();
            } else {
                error!(
                    report_room = %state.config.report_room,
                    "Not joined to report_room"
                );
            }
        }
        _ => {}
    }
}

#[tokio::main]
async fn main() -> Result<()> {
    tracing_subscriber::FmtSubscriber::builder()
        .with_env_filter(EnvFilter::new(
            // Default to RUST_LOG=info if not explicitly set
            std::env::var("RUST_LOG").unwrap_or_else(|_| "info".to_string()),
        ))
        .init();
    let home = dirs::home_dir().expect("unable to find home directory");
    let config: Config =
        toml::from_str(&fs::read_to_string(home.join("uatu.toml")).await?)?;
    let me = UserId::try_from(config.username.as_str())?;
    let client = Client::new_from_user_id_with_config(
        me.clone(),
        ClientConfig::new().store_path(home.join("store")),
    )
    .await?;

    client
        .login(me.localpart(), &config.password, None, Some("uatu"))
        .await?;

    debug!("Logged in");
    client.sync_once(SyncSettings::default()).await?;
    let state = State {
        config,
        recent_kicks: Arc::new(RwLock::new(collections::ExpiringSet::new(60))),
    };
    client
        .register_event_handler({
            let state = state.clone();
            move |ev: SyncStateEvent<MemberEventContent>,
                  client: Client,
                  room: Room| {
                let state = state.clone();
                async move {
                    handle_event(ev, client, room, state).await;
                }
            }
        })
        .await;
    info!("Finished sync_once, registered handlers");
    let settings =
        SyncSettings::default().token(client.sync_token().await.unwrap());
    client.sync(settings).await;
    Ok(())
}
