/**
Copyright (C) 2021 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use std::collections::HashMap;
use std::time::Instant;

/// A HashSet that automatically expires entries after
/// a specified number of seconds.
pub struct ExpiringSet {
    expiry: u64,
    inner: HashMap<String, Instant>,
}

impl ExpiringSet {
    pub fn new(expiry: u64) -> Self {
        Self {
            expiry,
            inner: Default::default(),
        }
    }

    /// Whether the set contains the key unexpired
    pub fn contains(&self, key: &str) -> bool {
        match self.inner.get(key) {
            Some(inst) => inst.elapsed().as_secs() < self.expiry,
            None => false,
        }
    }

    /// Add a new key to the set with a time of now,
    /// overriding any previously set time
    pub fn add(&mut self, key: &str) {
        self.inner.insert(key.to_string(), Instant::now());
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::thread::sleep;
    use std::time::Duration;

    #[test]
    fn test_expiringset() {
        let mut set = ExpiringSet::new(3);
        assert!(!set.contains("A"));
        set.add("A");
        assert!(set.contains("A"));
        sleep(Duration::from_secs(4));
        assert!(!set.contains("A"));
    }
}
