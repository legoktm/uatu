uatu
====

uatu is a Matrix bot that [watches](https://en.wikipedia.org/wiki/Uatu)
channels. It's designed to be a good  companion to
[mjolnir](https://github.com/matrix-org/mjolnir).

## Quick start

Create `~/uatu.toml`:
```toml
username = "@example:matrix.org"
password = "password"
report_room = "!aaaaaaaaaaaaa:matrix.org"
```

For now `report_room` needs to be the internal Matrix ID, not a room
address.

Make sure `~/store` is only readable by the current user.

## Features
* report kicked users to a central room
* report recently kicked users who join another room

Planned:
* report messages/users that match a regex pattern
* optionally auto report users to mjolnir

## License
uatu is released under the GPL v3, or any later version. See COPYING for
more details.
