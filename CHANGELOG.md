## 0.1.14 - 2024-05-21
* Mark project as needing a new maintainer

## 0.1.13 - 2022-08-21
* Ignore an IRC connection problem message
* Update dependencies

## 0.1.12 - 2022-04-18
* Update dependencies, especially regex for RUSTSEC-2022-0013.

## 0.1.11 - 2022-02-01
* Update thread_local for RUSTSEC-2022-0006

## 0.1.10 - 2022-01-16
* Update dependencies, especially sha2, which was yanked

## 0.1.9 - 2021-12-14
* Downgrade futures after release [was yanked](https://github.com/rust-lang/futures-rs/issues/2529).

## 0.1.8 - 2021-12-05
* Update tracing-subscriber to drop chrono dependency (RUSTSEC-2020-0159)

## 0.1.7 - 2021-11-21
* Update Tokio for RUSTSEC-2021-0124

## 0.1.6 - 2021-10-18
* Ignore kick when IRC bridge can't join channel
* Bump dependencies

## 0.1.5 - 2021-10-02
* Ignore 30 day IRC bridge inactivity kick

## 0.1.4 - 2021-10-01
* Report recently kicked users who rejoin another room
* Use Cargo's new resolver to avoid wasm dependencies
* Bump dependencies

## 0.1.3 - 2021-09-27
* Ignore some kicks from IRC appservice user that aren't real kicks
* Publish amd64 and armv7 binaries as GitLab releases
* Bump dependencies

## 0.1.2 - 2021-09-22
* Report what other rooms the kicked user is still in
* Bump dependencies

## 0.1.1 - 2021-09-21
* Adjust log levels
* Bump dependencies

## 0.1.0 - 2021-09-20
* Initial release
